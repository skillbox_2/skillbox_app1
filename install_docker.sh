#!/bin/bash
if [ -d "/etc/docker" ]; then 
echo "Docker installed!"
else
sudo apt update -y && sudo apt install apt-transport-https ca-certificates curl software-properties-common -y && curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - && sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"


sudo apt update -y && apt-cache policy docker-ce && sudo apt install docker-ce -y && sudo systemctl enable docker


sudo usermod -aG docker ${USER}

echo "Docker install, I am Good Boy"
fi
if [ -f "/usr/local/bin/docker-compose" ]; then
echo "Docker-compose installed"
else
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose && sudo chmod +x /usr/local/bin/docker-compose && sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
echo "Docker-compose install, I am good boy"
fi
sudo apt update
sudo apt install ansible -y

cd /opt/tmp

sudo docker build -t test1 .
sudo docker run -dp 8080:8080 test1